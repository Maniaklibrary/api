<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Book extends REST_Controller {

    function __construct() {
        parent::__construct();
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method == "OPTIONS") {
            die();
        }

        $this->load->model('BookModel');
    }

    function getAll_post() {
        
        $options['pageIndex'] = $this->security->xss_clean($this->post('pageIndex'));
        $options['pageSize'] = $this->security->xss_clean($this->post('pageSize'));
        
        //$options['pageIndex']++;

        $books = $this->BookModel->get($options);
        $total = $this->BookModel->total();

        $response = array('status' => true,
            'total' => $total,
            'options' => $options,
            'books' => $books);
        $this->response($response);
    }

    function new_post() {
        $book['name'] = $this->security->xss_clean($this->post('name'));
        $book['author'] = $this->security->xss_clean($this->post('author'));
        $book['publishDate'] = $this->security->xss_clean($this->post('publishDate'));
        $book['user'] = $this->security->xss_clean($this->post('user'));
        $category = $this->security->xss_clean($this->post('category'));

        $book['idCategory'] = $this->BookModel->getByName($category);
        if (!$book['idCategory']) {
            $response = array('status' => false, 'error' => "Category not found.");
            $this->response($response);
        }

        $this->BookModel->insert($book);

        $response = array('status' => true);
        $this->response($response);
    }

    function delete_post() {
        $book['id'] = $this->security->xss_clean($this->post('id'));

        $book['idCategory'] = $this->BookModel->delete($book['id']);

        $response = array('status' => true);
        $this->response($response);
    }

    function update_post() {
        $id = $this->security->xss_clean($this->post('id'));
        $book['name'] = $this->security->xss_clean($this->post('name'));
        $book['author'] = $this->security->xss_clean($this->post('author'));
        $book['publishDate'] = $this->security->xss_clean($this->post('publishDate'));
        $book['user'] = $this->security->xss_clean($this->post('user'));
        $category = $this->security->xss_clean($this->post('category'));

        $book['idCategory'] = $this->BookModel->getByName($category);
        if (!$book['idCategory']) {
            $response = array('status' => false, 'error' => "Category not found.");
            $this->response($response);
        }

        $this->BookModel->update($book, $id);

        $response = array('status' => true);
        $this->response($response);
    }

}
