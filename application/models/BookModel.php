<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BookModel extends CI_Model {

    public function get($options) {

        $this->load->database();

        $this->db->select('b.name, b.author, b.publishDate, b.user, b.id');
        $this->db->select('c.name as category');
        $this->db->from('Book b');
        $this->db->join('Category c', 'b.idCategory = c.id');
        $this->db->order_by('b.id','DESC');
        $this->db->limit( $options['pageSize'], ($options['pageSize'] * $options['pageIndex']) );
        $query = $this->db->get();

        return $query->result();
    }
    
    public function total() {

        $this->load->database();

        $this->db->select(' COUNT(*) as total ');
        $this->db->from('Book b');
        $this->db->join('Category c', 'b.idCategory = c.id');
        $query = $this->db->get();
        $total = $query->row_array();

        return $total['total'];
    }

    public function insert($book) {

        $this->load->database();

        $this->db->insert('Book', $book);
        $sesion['id'] = $this->db->insert_id();

        return $sesion;
    }

    public function getCategories() {

        $this->load->database();

        $this->db->select('*');
        $this->db->from('Category');
        $query = $this->db->get();

        return $query->result();
    }

    public function getByName($category) {

        if (!$category) {
            return null;
        }

        $this->load->database();

        $this->db->select('id');
        $this->db->from('Category');
        $this->db->where('name', $category);
        $query = $this->db->get();
        $category = $query->row_array();

        return $category['id'];
    }

    public function delete($id) {

        if (!$id) {
            return null;
        }

        $this->load->database();
        $this->db->where('id', $id);
        $this->db->delete('Book');
        $this->db->limit(1);
    }

    public function update($book, $id) {

        if (!$id) {
            return null;
        }

        $this->load->database();
        $this->db->where('id', $id);
        $this->db->update('Book', $book);
        $this->db->limit(1);
    }

}
